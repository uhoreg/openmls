use tls_codec::{TlsDeserialize, TlsSerialize, TlsSize};

use super::{
    Deserialize,
    Serialize,
};

/// # Generation Extension
///
/// The generation extension indicates the relative age of a key package.  Key
/// packages for the same user are newer if the generation is larger.
#[derive(PartialEq, Clone, Debug, Serialize, Deserialize, TlsSize, TlsSerialize, TlsDeserialize)]
pub struct GenerationExtension {
    generation: u64,
}

impl Default for GenerationExtension {
    fn default() -> Self {
        GenerationExtension {
            generation: 0,
        }
    }
}

impl GenerationExtension {
    /// Create a new generation extension with the given generation number.
    pub fn new(
        generation: u64,
    ) -> Self {
        Self {
            generation: generation,
        }
    }
    /// Get the generation number
    pub fn generation(&self) -> u64 {
        self.generation
    }
}
