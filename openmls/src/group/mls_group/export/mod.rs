//! Exported group state for decrypting historical messages

use crate::{
    group::*,
    key_packages::KeyPackage,
    prelude::{
        past_secrets::MessageSecretsStore,
        LeafNode,
        mls_group::ResumptionSecretStore,
        Node,
    },
    schedule::{
        message_secrets::MessageSecrets,
        AuthenticationSecret,
        ConfirmationKey,
        EncryptionSecret,
        ExporterSecret,
        ExternalSecret,
        GroupEpochSecrets,
        InitSecret,
        MembershipKey,
        ResumptionSecret,
        SenderDataSecret,
    },
    tree::{
        index::SecretTreeLeafIndex,
        treemath::*,
        secret_tree::SecretTree,
    },
    treesync::TreeSync,
    versions::ProtocolVersion,
};
use openmls_traits::types::Ciphersuite;
use tls_codec::Serialize as TlsSerializeTrait;

/// Export information about a group for decrypting historical messages
#[derive(Serialize, Deserialize)]
pub struct MlsGroupExport {
    mls_version: ProtocolVersion,
    ciphersuite: Ciphersuite,
    group_context: GroupContext,
    authentication_secret: AuthenticationSecret,
    sender_data_secret: SenderDataSecret,
    membership_key: MembershipKey,
    confirmation_key: ConfirmationKey,
    encryption_secret: EncryptionSecret,
    interim_transcript_hash: TlsByteVecU16,
    // or else RatchetTreeExtension? (which may already be part of GroupContext)
    key_packages: TlsVecU32<Option<KeyPackage>>,
    aad: TlsByteVecU16,
}

impl tls_codec::Size for MlsGroupExport {
    #[inline]
    fn tls_serialized_len(&self) -> usize {
        self.mls_version.tls_serialized_len()
            + self.ciphersuite.tls_serialized_len()
            + self.group_context.tls_serialized_len()
            + (2 + self.authentication_secret.secret.as_slice().len())
            + (2 + self.sender_data_secret.secret.as_slice().len())
            + (2 + self.membership_key.secret.as_slice().len())
            + (2 + self.confirmation_key.secret.as_slice().len())
            + (2 + self.encryption_secret.secret.as_slice().len())
            + self.interim_transcript_hash.tls_serialized_len()
            + self.key_packages.tls_serialized_len()
            + self.aad.tls_serialized_len()
    }
}

impl tls_codec::Serialize for MlsGroupExport {
    #[inline]
    fn tls_serialize<W: std::io::Write>(&self, writer: &mut W) -> Result<usize, tls_codec::Error> {
        let mut written = self.mls_version.tls_serialize(writer)?;
        written += self.ciphersuite.tls_serialize(writer)?;
        written += self.group_context.tls_serialize(writer)?;
        written += TlsByteVecU16::from(self.authentication_secret.secret.as_slice())
            .tls_serialize(writer)?;
        written += TlsByteVecU16::from(self.sender_data_secret.secret.as_slice())
            .tls_serialize(writer)?;
        written += TlsByteVecU16::from(self.membership_key.secret.as_slice())
            .tls_serialize(writer)?;
        written += TlsByteVecU16::from(self.confirmation_key.secret.as_slice())
            .tls_serialize(writer)?;
        written += TlsByteVecU16::from(self.encryption_secret.secret.as_slice())
            .tls_serialize(writer)?;
        written += self.interim_transcript_hash.tls_serialize(writer)?;
        written += self.key_packages.tls_serialize(writer)?;
        written += self.aad.tls_serialize(writer)?;
        Ok(written)
    }
}

impl tls_codec::Deserialize for MlsGroupExport {
    #[inline]
    fn tls_deserialize<R: std::io::Read>(bytes: &mut R) -> Result<Self, tls_codec::Error> {
        let mls_version = ProtocolVersion::tls_deserialize(bytes)?;
        let ciphersuite = Ciphersuite::tls_deserialize(bytes)?;
        let group_context = GroupContext::tls_deserialize(bytes)?;
        let authentication_secret = AuthenticationSecret {
            secret: Secret::from_slice(
                TlsByteVecU16::tls_deserialize(bytes)?
                    .as_slice(),
                mls_version,
                ciphersuite,
            ),
        };
        let sender_data_secret = SenderDataSecret {
            secret: Secret::from_slice(
                TlsByteVecU16::tls_deserialize(bytes)?
                    .as_slice(),
                mls_version,
                ciphersuite,
            ),
        };
        let membership_key = MembershipKey {
            secret: Secret::from_slice(
                TlsByteVecU16::tls_deserialize(bytes)?
                    .as_slice(),
                mls_version,
                ciphersuite,
            ),
        };
        let confirmation_key = ConfirmationKey {
            secret: Secret::from_slice(
                TlsByteVecU16::tls_deserialize(bytes)?
                    .as_slice(),
                mls_version,
                ciphersuite,
            ),
        };
        let encryption_secret = EncryptionSecret {
            secret: Secret::from_slice(
                TlsByteVecU16::tls_deserialize(bytes)?
                    .as_slice(),
                mls_version,
                ciphersuite,
            )
        };
        let interim_transcript_hash = TlsByteVecU16::tls_deserialize(bytes)?;
        let key_packages = TlsVecU32::<Option<KeyPackage>>::tls_deserialize(bytes)?;
        let aad = TlsByteVecU16::tls_deserialize(bytes)?;

        Ok(MlsGroupExport {
            mls_version,
            ciphersuite,
            group_context,
            authentication_secret,
            sender_data_secret,
            membership_key,
            confirmation_key,
            encryption_secret,
            interim_transcript_hash,
            key_packages,
            aad,
        })
    }
}

// FIXME: add proper errors

impl MlsGroup {
    /// Export the group information
    pub fn export_group(
        &self,
    ) -> Result<MlsGroupExport, ()> {
        let mut key_packages: Vec<Option<KeyPackage>> = Vec::new();

        for (index, node) in self.group.treesync().export_nodes().into_iter().enumerate() {
            match node {
                None => {
                    if index % 2 == 0 {
                        key_packages.push(None);
                    }
                },
                Some(Node::LeafNode(leaf_node)) => {
                    key_packages.push(Some(leaf_node.key_package().clone()));
                },
                Some(Node::ParentNode(_)) => {
                },
            }
        }

        let group_epoch_secrets = self.group.group_epoch_secrets();
        let authentication_secret = &group_epoch_secrets.exporter_secret().secret;
        let authentication_secret = AuthenticationSecret {
            secret: Secret::from_slice(
                authentication_secret.as_slice(),
                authentication_secret.version(),
                authentication_secret.ciphersuite(),
            ),
        };

        let message_secrets = self.group.message_secrets();
        let sender_data_secret = &message_secrets.sender_data_secret().secret;
        let membership_key = &message_secrets.membership_key().secret;
        let confirmation_key = &message_secrets.confirmation_key().secret;
        let secret_tree = &message_secrets.secret_tree;
        let secret_tree_root = root(secret_tree.size);
        let secret_tree_root_secret = &secret_tree.nodes[secret_tree_root.as_usize()];
        let encryption_secret = &secret_tree_root_secret.as_ref().ok_or(())?.secret;
        let encryption_secret = EncryptionSecret {
            secret: Secret::from_slice(
                encryption_secret.as_slice(),
                encryption_secret.version(),
                encryption_secret.ciphersuite(),
            ),
        };
        let sender_data_secret = SenderDataSecret {
            secret: Secret::from_slice(
                sender_data_secret.as_slice(),
                sender_data_secret.version(),
                sender_data_secret.ciphersuite(),
            ),
        };
        let membership_key = MembershipKey {
            secret: Secret::from_slice(
                membership_key.as_slice(),
                membership_key.version(),
                membership_key.ciphersuite(),
            ),
        };
        let confirmation_key = ConfirmationKey {
            secret: Secret::from_slice(
                confirmation_key.as_slice(),
                confirmation_key.version(),
                confirmation_key.ciphersuite(),
            ),
        };

        Ok(MlsGroupExport {
            mls_version: self.group.version(),
            ciphersuite: self.group.ciphersuite(),
            group_context: self.group.context().clone(),
            authentication_secret,
            sender_data_secret,
            membership_key,
            confirmation_key,
            encryption_secret,
            interim_transcript_hash: self.group.interim_transcript_hash().into(),
            key_packages: key_packages.into(),
            aad: self.aad.clone().into(),
        })
    }

    /// Create a group from an export
    pub fn create_from_export(
        backend: &impl OpenMlsCryptoProvider,
        export: MlsGroupExport,
        mls_group_config: &MlsGroupConfig,
    ) -> Result<MlsGroup, ()>  {
        let mut nodes: Vec<Option<Node>> = Vec::new();
        let mut kp_iter = export.key_packages.iter();
        match kp_iter.next() {
            Some(kp) => {
                match kp {
                    Some(kp) => {
                        nodes.push(Some(Node::LeafNode(
                            LeafNode::new(kp.clone(), backend.crypto()).or(Err(()))?,
                        )));
                    },
                    None => {
                        nodes.push(None);
                    },
                }
            },
            None => {
            },
        }
        loop {
            match kp_iter.next() {
                Some(kp) => {
                    nodes.push(None);
                    match kp {
                        Some(kp) => {
                            nodes.push(Some(Node::LeafNode(
                                LeafNode::new(kp.clone(), backend.crypto()).or(Err(()))?,
                            )));
                        },
                        None => {
                            nodes.push(None);
                        },
                    }
                },
                None => {
                    break;
                },
            }
        }
        /*
        should be equivalent to:

            let nodes = kp_iter.map(|kp| {
                match kp {
                    Some(kp) => {
                        Some(Node::LeafNode(
                            LeafNode::new(kp.clone(), backend.crypto()).unwrap(),
                        ))
                    },
                    None => {
                        None
                    },
                }
            })
                .intersperse(None)
                .collect::<Vec<_>>();

        but .intersperse is an unstable feature
        */
        let tree = TreeSync::from_nodes_without_leaf(
            backend,
            export.ciphersuite,
            nodes,
        )
            .map_err(|_| ())?;
        let serialized_context = export.group_context
            .tls_serialize_detached()
            .or(Err(()))?;
        let group_epoch_secrets = GroupEpochSecrets {
            init_secret: InitSecret {
                secret: Secret::default(),
            },
            exporter_secret: ExporterSecret {
                secret: Secret::default(),
            },
            authentication_secret: AuthenticationSecret {
                secret: Secret::from_slice(
                    export.authentication_secret.as_slice(),
                    export.mls_version,
                    export.ciphersuite,
                ),
            },
            external_secret: ExternalSecret {
                secret: Secret::default(),
            },
            resumption_secret: ResumptionSecret {
                secret: Secret::default(),
            },
        };
        let message_secrets = MessageSecrets::new(
            export.sender_data_secret,
            export.membership_key,
            export.confirmation_key,
            Vec::from(serialized_context),
            SecretTree::new(
                export.encryption_secret,
                export.key_packages.len().into(),
                SecretTreeLeafIndex(u32::MAX),
            ),
        );
        let group = CoreGroup {
            ciphersuite: export.ciphersuite,
            group_context: export.group_context,
            group_epoch_secrets,
            tree,
            interim_transcript_hash: export.interim_transcript_hash.into(),
            use_ratchet_tree_extension: true,
            mls_version: export.mls_version,
            message_secrets_store: MessageSecretsStore::new_with_secret(0, message_secrets),
        };
        Ok(MlsGroup {
            mls_group_config: mls_group_config.clone(),
            group,
            proposal_store: ProposalStore::new(),
            own_kpbs: Vec::new(),
            aad: export.aad.clone().into(),
            resumption_secret_store: ResumptionSecretStore::new(0),
            group_state: MlsGroupState::Operational,
            state_changed: InnerState::Persisted,
        })
    }
}
